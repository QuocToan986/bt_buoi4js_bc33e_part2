/*********************bài 1*****************/
document.getElementById("btnNgayHomQua").onclick = function () {
  var numberNgay = Number(document.getElementById("txt-ngay").value);
  var numberThang = Number(document.getElementById("txt-thang").value);
  var numberNam = Number(document.getElementById("txt-nam").value);

  // gán ngày tối đa
  var ngayCuaThang = 0;
  switch (numberThang) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      {
        ngayCuaThang = 31;
      }
      break;
    case 2:
      {
        ngayCuaThang = 28;
      }
      break;
    case 4:
    case 6:
    case 9:
    case 11:
      {
        ngayCuaThang = 30;
      }
      break;
  }
  // check điều kiện
  if (numberNam < 1) {
    document.getElementById("thongBao1").innerHTML = "Số năm không hợp lệ!";
    return;
  }

  if (numberThang < 1 || numberThang > 12) {
    document.getElementById("thongBao1").innerHTML = "Số tháng không hợp lệ!";
    return;
  }

  if (numberNgay > ngayCuaThang) {
    document.getElementById("thongBao1").innerHTML = "Số ngày không hợp lệ";
    return;
  }

  // giảm ngày
  numberNgay--;
  if (numberNgay == 0) {
    numberThang--;
    if (numberThang == 0) {
      numberThang = 12;
      numberNam--;
    }
    // gán ngày tối đa
    var ngayCuaThang = 0;
    switch (numberThang) {
      case 1:
      case 3:
      case 5:
      case 7:
      case 8:
      case 10:
      case 12:
        {
          ngayCuaThang = 31;
        }
        break;
      case 2:
        {
          ngayCuaThang = 28;
        }
        break;
      case 4:
      case 6:
      case 9:
      case 11:
        {
          ngayCuaThang = 30;
        }
        break;
    }
    numberNgay = ngayCuaThang;
  }
  document.getElementById("thongBao1").innerHTML =
    numberNgay + "/" + numberThang + "/" + numberNam;
};
document.getElementById("btnNgayMai").onclick = function () {
  var numberNgay = Number(document.getElementById("txt-ngay").value);
  var numberThang = Number(document.getElementById("txt-thang").value);
  var numberNam = Number(document.getElementById("txt-nam").value);

  // gán ngày tối đa
  var ngayCuaThang = 0;
  switch (numberThang) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      {
        ngayCuaThang = 31;
      }
      break;
    case 2:
      {
        ngayCuaThang = 28;
      }
      break;
    case 4:
    case 6:
    case 9:
    case 11:
      {
        ngayCuaThang = 30;
      }
      break;
  }
  // check điều kiện
  if (numberNam < 1) {
    document.getElementById("thongBao1").innerHTML = "Số năm không hợp lệ!";
    return;
  }

  if (numberThang < 1 || numberThang > 12) {
    document.getElementById("thongBao1").innerHTML = "Số tháng không hợp lệ!";
    return;
  }

  if (numberNgay > ngayCuaThang || numberNgay < 1) {
    document.getElementById("thongBao1").innerHTML = "Số ngày không hợp lệ";
    return;
  }
  //  tăng ngày
  numberNgay++;
  if (numberNgay > ngayCuaThang) {
    numberNgay = 1;
    numberThang++;
    if (numberThang > 12) {
      numberThang = 1;
      numberNam++;
    }
  }
  document.getElementById("thongBao1").innerHTML =
    numberNgay + "/" + numberThang + "/" + numberNam;
};

/*********************bài 2*****************/
document.getElementById("btnTinhNgay").onclick = function () {
  var nMonth = document.getElementById("txtMonth").value * 1;
  var nYear = document.getElementById("txtYear").value * 1;
  // kiểm tra input
  if (nMonth == "" && nYear == "") {
    document.getElementById("thongBao2").innerHTML = "Nhập input hợp lệ!";
    return;
  } else if ((nMonth > 1 && nMonth > 12) || nMonth == "") {
    document.getElementById("thongBao2").innerHTML = "Số tháng không hợp lệ!";
    return;
  } else if (nYear < 1) {
    document.getElementById("thongBao2").innerHTML = "Số năm không hợp lệ!";
    return;
  }

  // kiệm tra năm nhuận
  // năm % 4 === 0 && năm % 100 !== 0 || năm % 400 === 0
  isLeapYeaer = false;
  if ((nYear % 4 === 0 && nYear % 100 !== 0) || nYear % 400 === 0) {
    isLeapYeaer = true;
  }
  // gán ngày cho tháng
  var dayOfMonth = 0;
  switch (nMonth) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      {
        dayOfMonth = 31;
      }
      break;
    case 2:
      {
        if (isLeapYeaer == true) {
          dayOfMonth = 29;
        } else {
          dayOfMonth = 28;
        }
      }
      break;
    case 4:
    case 6:
    case 9:
    case 11:
      {
        dayOfMonth = 30;
      }
      break;
  }
  document.getElementById("thongBao2").innerHTML = dayOfMonth;
};

/*********************bài 3*****************/
document.getElementById("btn-read").onclick = function () {
  //  input: số nguyên 3 chữ số
  var number = document.getElementById("txt-number").value * 1;
  var hangTram = number / 100;
  hangTram = Math.floor(hangTram);
  var hangChuc = (number / 10) % 10;
  hangChuc = Math.floor(hangChuc);
  var hangDv = number % 10;
  // output: cách đọc số đó
  var inHangTram = "";
  var inHangChuc = "";
  var inHangDv = "";
  //proccess:
  //hàng trăm
  switch (hangTram) {
    case 1: {
      inHangTram = "Một trăm";
      break;
    }
    case 2: {
      inHangTram = "Hai trăm";
      break;
    }
    case 3: {
      inHangTram = "Ba trăm";
      break;
    }
    case 4: {
      inHangTram = "Bốn trăm";
      break;
    }
    case 5: {
      inHangTram = "Năm trăm";
      break;
    }
    case 6: {
      inHangTram = "Sáu trăm";
      break;
    }
    case 7: {
      inHangTram = "Bảy trăm";
      break;
    }
    case 8: {
      inHangTram = "Tám trăm";
      break;
    }
    case 9: {
      inHangTram = "Chín trăm";
      break;
    }
    default: {
      inHangTram = "nhập từ số từ 100 tới 999";
    }
  }
  // hàng chục
  switch (hangChuc) {
    case 1: {
      inHangChuc = "mười";
      break;
    }
    case 2: {
      inHangChuc = "hai mươi";
      break;
    }
    case 3: {
      inHangChuc = "ba mươi";
      break;
    }
    case 4: {
      inHangChuc = "bốn mươi";
      break;
    }
    case 5: {
      inHangChuc = "năm mươi";
      break;
    }
    case 6: {
      inHangChuc = "sáu mươi";
      break;
    }
    case 7: {
      inHangChuc = "bảy mươi";
      break;
    }
    case 8: {
      inHangChuc = "tám mươi";
      break;
    }
    case 9: {
      inHangChuc = "chín mươi";
      break;
    }
    default: {
      inHangChuc = "nhập từ số từ 100 tới 999";
    }
  }
  // hàng chục
  switch (hangDv) {
    case 1: {
      inHangDv = "mười";
      break;
    }
    case 2: {
      inHangDv = "hai";
      break;
    }
    case 3: {
      inHangDv = "ba";
      break;
    }
    case 4: {
      inHangDv = "bốn";
      break;
    }
    case 5: {
      inHangDv = "năm";
      break;
    }
    case 6: {
      inHangDv = "sáu";
      break;
    }
    case 7: {
      inHangDv = "bảy";
      break;
    }
    case 8: {
      inHangDv = "tám";
      break;
    }
    case 9: {
      inHangDv = "chín";
      break;
    }
    default: {
      inHangDv = "nhập từ số từ 100 tới 999";
    }
  }
  var thongBaoIn = inHangTram + " " + inHangChuc + " " + inHangDv;
  //xuất thông báo
  document.getElementById("thongBao").innerHTML = thongBaoIn;
};

/*********************bài 4*****************/
document.getElementById("btnTim").onclick = function () {
  var resultSV1 = 0;
  var resultSV2 = 0;
  var resultSV3 = 0;

  var sinhVien1 = document.getElementById("txt-sv1").value;
  var sinhVien1X = document.getElementById("txt-sv1-x").value * 1;
  var sinhVien1Y = document.getElementById("txt-sv1-y").value * 1;

  var sinhVien2 = document.getElementById("txt-sv2").value;
  var sinhVien2X = document.getElementById("txt-sv2-x").value * 1;
  var sinhVien2Y = document.getElementById("txt-sv2-y").value * 1;

  var sinhVien3 = document.getElementById("txt-sv3").value;
  var sinhVien3X = document.getElementById("txt-sv3-x").value * 1;
  var sinhVien3Y = document.getElementById("txt-sv3-y").value * 1;

  var truongX = document.getElementById("txt-X-truong").value * 1;
  var truongY = document.getElementById("txt-Y-truong").value * 1;

  resultSV1 =
    Math.pow(truongX, 2) -
    Math.pow(sinhVien1X, 2) +
    Math.pow(truongY, 2) -
    Math.pow(sinhVien1Y, 2);
  if (resultSV1 < 0) {
    resultSV1 = resultSV1 * -1;
  }
  resultSV1 = Math.sqrt(resultSV1);

  resultSV2 =
    Math.pow(truongX, 2) -
    Math.pow(sinhVien2X, 2) +
    Math.pow(truongY, 2) -
    Math.pow(sinhVien2Y, 2);
  if (resultSV2 < 0) {
    resultSV2 = resultSV2 * -1;
  }
  resultSV2 = Math.sqrt(resultSV2);

  resultSV3 =
    Math.pow(truongX, 2) -
    Math.pow(sinhVien3X, 2) +
    Math.pow(truongY, 2) -
    Math.pow(sinhVien3Y, 2);
  if (resultSV3 < 0) {
    resultSV3 = resultSV3 * -1;
  }
  resultSV3 = Math.sqrt(resultSV3);

  if (resultSV1 > resultSV2 && resultSV1 > resultSV3) {
    document.getElementById("thongBao4").innerHTML =
      "Sinh viên xa trường nhất" + ":" + " " + sinhVien1;
  } else if (resultSV2 > resultSV1 && resultSV2 > resultSV3) {
    document.getElementById("thongBao4").innerHTML =
      "Sinh viên xa trường nhất" + ":" + " " + sinhVien2;
  } else if (resultSV3 > resultSV1 && resultSV3 > resultSV2) {
    document.getElementById("thongBao4").innerHTML =
      "Sinh viên xa trường nhất" + ":" + " " + sinhVien3;
  }
};
